//
//  SearchPhotosJSON.swift
//  VirtualTourists
//
//  Created by Volodymyr Mykhailiuk on 27.11.2019.
//  Copyright © 2019 Volodymyr Mykhailiuk. All rights reserved.
//

struct SearchPhotosJSON: Decodable {
    let photos: PhotosJSON
}
